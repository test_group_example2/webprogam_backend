import { IsNotEmpty, Matches, IsPositive } from 'class-validator';
export class CreateProductDto {
  @Matches(/.{8,}$/)
  @IsNotEmpty()
  name: string;
  @IsPositive()
  @IsNotEmpty()
  price: number;
}
